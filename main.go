package main

import (
	"encoding/csv"
	"fmt"
	"io"
	"math/big"
	"net"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"
)

type IP struct {
	version string
	value   *big.Int
}

func ParseIP(ip string) IP {
	version := "IPv4"
	if strings.Contains(ip, ":") {
		version = "IPv6"
	}

	return IP{
		version: version,
		value:   new(big.Int).SetBytes(net.ParseIP(ip)),
	}
}

type IPRange struct {
	start   IP
	end     IP
	country string
}

func (r *IPRange) Includes(ip IP) bool {
	return ip.value.Cmp(r.start.value) >= 0 && ip.value.Cmp(r.end.value) <= 0
}

type CacheRegister struct {
	loadedAt time.Time
	content  []IPRange
}

func (c *CacheRegister) Set(content []IPRange) {
	c.loadedAt = time.Now()
	c.content = content
}
func (c *CacheRegister) Invalidate() {
	c.loadedAt = nilTime
	c.content = nil
}
func (c *CacheRegister) IsWarm() bool {
	return !c.loadedAt.IsZero() && c.loadedAt.After(time.Now().Add(-cacheTTL))
}

var (
	nilTime time.Time

	sources = map[string]string{
		"IPv4": "https://cdn.jsdelivr.net/npm/@ip-location-db/asn-country/asn-country-ipv4.csv",
		"IPv6": "https://cdn.jsdelivr.net/npm/@ip-location-db/asn-country/asn-country-ipv6.csv",
	}

	cacheTTL = 24 * time.Hour

	caches = map[string]CacheRegister{
		"IPv4": CacheRegister{},
		"IPv6": CacheRegister{},
	}
)

func main() {
	if len(os.Args) == 1 {
		fmt.Println("=== ipvx ===")
		fmt.Println("minimalistic IP country lookup")
		fmt.Println("author: andrea vos – avris.it")
		fmt.Println("license: OQL – oql.avris.it")
		fmt.Println("usage:")
		fmt.Println(" – ipvx [port] – starts an HTTP server on the specified port")
		fmt.Println(" – ipvx [ipv4] – looks up the country code for the given IPv4 and prints it to stdout")
		fmt.Println(" – ipvx [ipv6] – looks up the country code for the given IPv6 and prints it to stdout")
		return
	}

	if port, err := strconv.Atoi(os.Args[1]); err == nil {
		http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
			fmt.Println(r.URL.Path)
			country, err := FindIpCountry(ParseIP(r.URL.Path[1:]))
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			fmt.Fprintf(w, country)
		})

		fmt.Println(fmt.Sprintf("Starting a server on port %v", port))
		err := http.ListenAndServe(fmt.Sprintf(":%v", port), nil)
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
		}
		return
	}

	country, err := FindIpCountry(ParseIP(os.Args[1]))
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		return
	}
	fmt.Println(country)
}

func FindIpCountry(ip IP) (string, error) {
	exePath, err := os.Getwd()
	if err != nil {
		return "XX", err
	}
	database := filepath.Join(exePath, ip.version+".csv")

	cc := caches[ip.version]
	cache := &cc

	if !HasFreshFile(database) {
		err = DownloadFile(sources[ip.version], database)
		if err != nil {
			return "XX", err
		}
		cache.Invalidate()
	}

	if !cache.IsWarm() {
		rows, err := ReadCsv(database)
		if err != nil {
			return "XX", err
		}

		cache.Set(LoadRanges(rows))
	}

	for _, rng := range cache.content {
		if rng.Includes(ip) {
			return rng.country, nil
		}
	}

	return "XX", nil
}

func HasFreshFile(file string) bool {
	fileInfo, err := os.Stat(file)
	return err == nil && fileInfo.ModTime().After(time.Now().Add(-cacheTTL))
}

func DownloadFile(url, outputFile string) error {
	response, err := http.Get(url)
	if err != nil {
		return err
	}
	defer response.Body.Close()

	file, err := os.Create(outputFile)
	if err != nil {
		return err
	}
	defer file.Close()

	_, err = io.Copy(file, response.Body)
	if err != nil {
		return err
	}

	return nil
}

func ReadCsv(fileName string) ([][]string, error) {
	file, err := os.Open(fileName)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	reader := csv.NewReader(file)

	return reader.ReadAll()
}

func LoadRanges(rows [][]string) []IPRange {
	var ranges []IPRange
	for _, row := range rows {
		ranges = append(ranges, IPRange{
			start:   ParseIP(row[0]),
			end:     ParseIP(row[1]),
			country: row[2],
		})
	}
	return ranges
}
