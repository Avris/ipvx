.DEFAULT_GOAL := build-all
APP_NAME := ipvx
PLATFORMS := windows-amd64 darwin-amd64 darwin-arm64 linux-amd64
OUT_DIR := bin

build-all: clean
	@for platform in $(PLATFORMS); do \
		echo "Building $$platform"; \
		$(MAKE) build GOOS=$$(echo $$platform | cut -d '-' -f 1) GOARCH=$$(echo $$platform | cut -d '-' -f 2); \
	done

build:
	go build -o $(OUT_DIR)/$(APP_NAME)-$(GOOS)-$(GOARCH)

clean:
	rm -rf $(OUT_DIR)

help:
	@echo "Available targets:"
	@echo "  build-all  : Build executables for all target platforms"
	@echo "  build      : Build the executable for the current platform"
	@echo "  clean      : Clean the output directory"
	@echo "  help       : Show this help message"
