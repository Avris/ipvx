# ipvx

> minimalistic IP country lookup

this script:
 - downloads the ASN database ([CC0](https://creativecommons.org/share-your-work/public-domain/cc0/))
   from [ip-location-db](https://github.com/sapics/ip-location-db) and keeps it up to date
 - parses that database and uses it to look up the country of of the IPv4 and IPv6 addresses you provide

simple as that.

## installation

clone the repository and run the Go script with `go run main.go`
or simply head to the [/bin](./bin) directory and download the binary for your OS and architecture

you might want to set up a symlink in order to be able to simply access `ipvx` system-wide, for example:

    sudo ln -s /home/andrea/ipvx/bin/ipvx-linux-amd64 /usr/local/bin/ipvx

## usage

to look up an IP address in the terminal, run:

    ipvx 123.45.67.89
    ipvx 2606:4700:4700::1111

to start an HTTP server on port 8111, run:

    ipvx 8111

and then request for example:

    > curl http://127.0.0.1:8111/23.45.67.89
    US

## building binaries

    make

## author & links

 - andrea vos
 - [avris.it](https://avris.it)
 - license: [OQL](https://oql.avris.it/license?c=Andrea%20Vos%7Chttps://avris.it)
 - [source code](https://gitlab.com/Avris/ipvx)
